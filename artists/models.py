from email.policy import default
from unicodedata import name
from unittest.util import _MAX_LENGTH
from django.db import models
from django.forms import CharField

# Create your models here.
class Artist (models.Model):
      stage_name= models.CharField(max_length=200,unique=True,blank=False,primary_key=True)
      social_link=models.TextField(blank=True,null=False)

class Album(models.Model):
    artist = models.ForeignKey(Artist,blank=False)
    name=models.CharField(default='New Album')
    create_time=models.DateTimeField(auto_now_add=True)
    realese_time=models.DateTimeField(editable=True,blank=False)
    cost=models.FloatField(blank=False)